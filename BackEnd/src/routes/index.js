const { Router } = require('express');

const router = new Router();

router.get('/', (req, res) => {
    const data = {
        name: 'SA',
        website: 'Software Avanzado - Original - Practica 4 - A '
    };
    res.json(data);
});  

module.exports = router;
